/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.gplateforme.web.rest.vm;
